import * as defs from './definitions';
import _ from 'lodash';

export function validateCol(col) {
	if( undefined === col.content ){
		col.content = defs.col.content;
	}

	if( undefined === col.type || -1 == defs.colTypes.indexOf(col.type)){
		col.type = defs.col.type;
	}

	return _.pick( col, Object.keys( defs.col ) );
}

export function validateCols(cols) {
	_.forEach( cols, function (col,i) {
		cols[i] = validateCol(col);
		cols[i].ID = i;
	} );
	return cols;
}


export function validateRows( rows ) {
	_.forEach( rows, function ( row, i ) {
		var layout = defs.rowLayoutTypes.find( type => type.label == row.layout );
		if( undefined === row.layout || undefined == layout ){
			layout = row.layout = defs.row.layout;
		}
		if( ! _.isEmpty( rows[i].columns ) ){
			var ci = 0;
			layout.widths.forEach( function (width) {
				if( _.isEmpty(rows[i].columns[ci] ) ){
					rows[i].columns[ci] = validateCol( {} );

				}else{
					rows[i].columns[ci] = validateCol( rows[i].columns[ci] );
					rows[i].columns[ci].width = width;
				}
				rows[i].columns[ci].width = width;

				ci++;
			});

			_.map(rows[i].columns, function (col,c) {
				if( rows[i].columns.length < c ){
					rows[i].columns[c].width = 0;
				}
			});

		}else{
			rows[i].columns = [];
		}
		row = _.pick( row, Object.keys( defs.row ) );
		
	});


	return rows;
}

export function rowLayoutOptions() {
	return defs.rowLayoutTypes.map(function(a) {return a.label });

}