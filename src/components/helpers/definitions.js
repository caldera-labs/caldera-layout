export const col = {
	content: '',
	type: 'content', //changer back to textarea
};

export const colTypes = [
	'content',
	'image',
	'textarea',
];

const layout12 ={
	widths: [12],
	label: '12'
};

const layout333 = {
	widths: [3, 3, 3],
	label: '3|3|3'
};

const layout66 = {
	widths: [6,6],
	label: '6|6'
};

const layout39 = {
	widths: [3,9],
	label: '3|9'
};

const layout93 = {
		widths: [9,3],
		label: '9|3'
};

export const rowLayoutTypes = [
	layout12,
	layout333,
	layout66,
	layout39,
	layout93
];

export const row = {
	name: 'Row ',
	layout: layout66,
	columns: [],
};