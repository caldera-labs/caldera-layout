import Vue from 'vue';
import Vuex from 'vuex';
import _ from 'lodash';
import * as validators from '../components/helpers/validators';
import * as defs from '../components/helpers/definitions';

Vue.use(Vuex);




/* eslint-disable no-new */
export default new Vuex.Store({

	state: {
		count: 0,
		layouts : [
			{
				ID: 1,
				name: 'Layouts 1',
				rows: [
					{
						columns:[
							{
								name: 'twelver',
								width: 12,
							},

						]

					}
				]
			},
			{
				ID: 2,
				name: 'Layout 2',
				rows: [
					{
						name: 'Top',
						layout: '',
						columns: [
							{
								content: 'Twelver Row 1',
								width: 12,
								type: 'content'
							},

						],
					},
					{
						name: 'Second',
						layout: '3|3|3',
						columns: [
							{
								content: 'Globe Logo',
								width: 45,
								type: 'image',
							},
							{
								content: 'n9',
								width: -9,
							},
							{
								content: '5',
								width: 5,
							},
							{
								content: 'rand',
								coo: 1
							}
						]
					}
				]



			}

		],
		images : [
			{
				name: 'Globe Logo',
				alt: 'Caldera Forms Globe Logo',
				url: 'https://github.com/CalderaWP/caldera-template-assets/raw/master/globe-logo/globe-white-bg-1200.png',
			},
			{
				name: 'Banner',
				alt: 'Caldera Forms Banner',
				url: 'https://github.com/CalderaWP/caldera-template-assets/raw/master/images/cf-name-banner-grey-cropped.png'
			}
		]
	},
	mutations: {
		updateLayout (state, layout) {
			let ID = layout.ID;
			let index = state.layouts.findIndex(layout => layout.ID === ID);
			if( -1 === index ){
				state.layouts.push( layout );
			}else{
				state.layouts[index] = layout;
			}
		},
		addRow(state, layout) {
			let ID = layout.ID;
			let index = state.layouts.findIndex(layout => layout.ID === ID);
			if( -1 === index ) {
				throw 'Layout not found in store';
			}
			if( ! layout.hasOwnProperty('rows' ) ){
				layout.rows = [];
			}

			layout.rows.push( defs.row );
			state.layouts[index] = layout;
		}

	},
	getters: {
		getLayoutById: (state, getters) => (ID) => {
			let layout = state.layouts.find(layout => layout.ID === ID);
			layout.rows = validators.validateRows(layout.rows);
			return layout;
		},
		getRows: (state,getters) => (ID)=>{
			let layout = state.layouts.find(layout => layout.ID === ID);
			if( ! layout.hasOwnProperty( 'rows' ) ){
				layout.rows = {};
			}

			return validators.validateRows(layout.rows);
		},
		getImage: (state,getters) => (name) => {
			return state.images.find(image => image.name === name );
		},
		allImages: (state,getters) =>(x) =>{
			return state.images;
		}
	}

});

