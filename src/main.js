// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

Vue.config.productionTip = false;

import store from './store/index';
import VueHtml5Editor from 'vue-html5-editor'
Vue.use(VueHtml5Editor,{
  showModuleName: true,
  image: {
    sizeLimit: 512 * 1024,
    compress: true,
    width: 500,
    height: 500,
    quality: 80
  }
});

import Sortable from 'vue-sortable';

Vue.use(Sortable);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
